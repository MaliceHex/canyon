﻿using Canyon.Ai.Managers;
using Canyon.Ai.Sockets;
using Canyon.Ai.Sockets.Packets;
using Canyon.Database.Entities;
using Canyon.Network.Packets.Ai;
using System.Collections.Concurrent;
using System.Drawing;

namespace Canyon.Ai.States.World
{
    public sealed class Generator
    {
        private static readonly ILogger logger = LogFactory.CreateLogger<Generator>();
        private static uint idGenerator = 2000000;

        private readonly DbGenerator generator;
        private readonly DbMonstertype monstertype;
        private readonly Point center;
        private readonly GameMap gameMap;

        private readonly ConcurrentDictionary<uint, TimeOut> awaitingReborn = new();
        private readonly ConcurrentDictionary<uint, Monster> monsters = new();

        private readonly TimeOut timeBetweenGens = new(2);

        public Generator(DbGenerator generator)
        {
            this.generator = generator;
            center = new Point(generator.BoundX + generator.BoundCx / 2, generator.BoundY + generator.BoundCy / 2);

            gameMap = MapManager.GetMap(generator.Mapid);
            if (gameMap == null)
            {
                logger.LogError($"Could not load map [{generator.Mapid}] for generator [{generator.Id}]");
                return;
            }


            if (generator.Npctype != 0)
            {
                monstertype = RoleManager.GetMonstertype(generator.Npctype);
                if (monstertype == null)
                {
                    logger.LogError($"Could not load monstertype [{generator.Npctype}] for generator [{generator.Id}]");
                    return;
                }
            }
        }

        public Generator(uint idMap, uint idMonster, ushort usX, ushort usY, ushort usCx, ushort usCy)
        {
            generator = new DbGenerator
            {
                Mapid = idMap,
                BoundX = usX,
                BoundY = usY,
                BoundCx = usCx,
                BoundCy = usCy,
                Npctype = idMonster,
                MaxNpc = 0,
                MaxPerGen = 0,
                Id = idGenerator++
            };

            center = new Point(generator.BoundX + generator.BoundCx / 2, generator.BoundY + generator.BoundCy / 2);

            gameMap = MapManager.GetMap(generator.Mapid);
            if (gameMap == null)
            {
                logger.LogError($"Could not load map [{generator.Mapid}] for generator [{generator.Id}]");
                return;
            }

            if (idMonster != 0)
            {
                monstertype = RoleManager.GetMonstertype(generator.Npctype);
                if (monstertype == null)
                {
                    logger.LogError($"Could not load monstertype [{generator.Npctype}] for generator [{generator.Id}]");
                    return;
                }
            }
        }

        public int Generated => monsters.Count;

        public bool Ready => (generator.Npctype == 0 || monstertype != null) && gameMap != null;

        public bool IsActive => gameMap.PlayerCount > 0 || monsters.Values.Any(x => !x.IsAlive || x.Map?.IsInstanceMap == true);

        public uint Identity => generator.Id;

        public uint RoleType => generator.Npctype;

        public int RestSeconds => Math.Max(generator.RestSecs, MIN_TIME_BETWEEN_GEN);

        public uint MapIdentity => generator.Mapid;

        public string MonsterName => monstertype.Name;

        public bool Add(Monster monster)
        {
            return monsters.TryAdd(monster.Identity, monster);
        }

        public async Task<Monster> GenerateMonsterAsync()
        {
            uint identity = (uint)IdentityManager.Monster.GetNextIdentity;
            var monster = new Monster(monstertype, identity, this);
            Point pos = await gameMap.QueryRandomPositionAsync(generator.BoundX, generator.BoundY, generator.BoundCx, generator.BoundCy);

            if (pos == default || !await monster.InitializeAsync(MapIdentity, (ushort)pos.X, (ushort)pos.Y))
            {
                IdentityManager.Monster.ReturnIdentity(monster.Identity);
                return null;
            }

            return monster;
        }

        public async Task<int> GenerateAsync()
        {
            if (!IsActive)
            {
                return 0;
            }

            foreach (var monster in awaitingReborn.Where(x => x.Value.IsTimeOut()))
            {
                monsters.TryRemove(monster.Key, out _);
                awaitingReborn.TryRemove(monster.Key, out _);
            }

            if (timeBetweenGens.ToNextTime())
            {
                int generate = Math.Min(generator.MaxPerGen - Generated, Math.Min(MAX_PER_GEN, generator.MaxNpc));
                if (generate > 0)
                {
                    while (generate-- > 0)
                    {
                        Monster monster = await GenerateMonsterAsync();
                        if (monster == null || !monsters.TryAdd(monster.Identity, monster))
                            continue;
                        await monster.EnterMapAsync();
                    }
                }
            }

            return await OnTimerAsync();
        }

        public async Task<int> OnTimerAsync()
        {
            if (gameMap.IsRaceTrack())
            {
                return 0;
            }

            int count = 0;
            foreach (var monster in monsters.Values.Where(x => x.IsAlive))
            {
                await monster.OnTimerAsync();
                count++;
            }
            return count;
        }

        public int SendAll()
        {
            var result = 0;
            MsgAiSpawnNpc msg = new();
            msg.Mode = AiSpawnNpcMode.Spawn;
            foreach (Monster npc in monsters.Values.Where(x => x.IsAlive))
            {
                if (msg.List.Count >= 25)
                {
                    result += msg.List.Count;
                    BroadcastMsg(msg);
                    msg.List.Clear();
                }

                msg.List.Add(new MsgAiSpawnNpc<GameServer>.SpawnNpc
                {
                    Id = npc.Identity,
                    GeneratorId = Identity,
                    MonsterType = npc.Type,
                    MapId = npc.MapIdentity,
                    X = npc.X,
                    Y = npc.Y
                });
            }

            if (msg.List.Count > 0)
            {
                result += msg.List.Count;
                BroadcastMsg(msg);
            }

            return result;
        }

        public Point GetCenter()
        {
            return center;
        }

        public bool IsTooFar(ushort x, ushort y, int nRange)
        {
            return !(x >= generator.BoundX - nRange
                     && x < generator.BoundX + generator.BoundCx + nRange
                     && y >= generator.BoundY - nRange
                     && y < generator.BoundY + generator.BoundCy + nRange);
        }

        public bool IsInRegion(int x, int y)
        {
            return x >= generator.BoundX && x < generator.BoundX + generator.BoundCx
                                       && y >= generator.BoundY && y < generator.BoundY + generator.BoundCy;
        }

        public int GetWidth()
        {
            return generator.BoundCx;
        }

        public int GetHeight()
        {
            return generator.BoundCy;
        }

        public int GetPosX()
        {
            return generator.BoundX;
        }

        public int GetPosY()
        {
            return generator.BoundY;
        }

        public void Remove(uint role)
        {
            if (generator.MaxPerGen == 0)
            {
                monsters.TryRemove(role, out _);
                return;
            }

            if (monsters.TryGetValue(role, out Monster mob))
            {
                var tm = new TimeOut();
                tm.Startup(RestSeconds);
                awaitingReborn.TryAdd(role, tm);
            }
        }

        private const int MAX_PER_GEN = 15;
        private const int MIN_TIME_BETWEEN_GEN = 10;
    }
}
