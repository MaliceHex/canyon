﻿namespace Canyon.Shared
{
    public static class UnixTimestamp
    {

        public static int Now => (int)(DateTime.Now - DateTime.UnixEpoch).TotalSeconds;

        public static DateTime ToDateTime(int seconds) => DateTime.UnixEpoch.AddSeconds(seconds);

        public static int FromDateTime(DateTime dateTime) => (int)(dateTime - DateTime.UnixEpoch).TotalSeconds;

    }
}
