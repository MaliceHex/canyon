using System.Net;
using System.Net.Sockets;
using Canyon.Shared;
using Microsoft.Extensions.Logging;
using StreamJsonRpc;

namespace Canyon.Network.RPC
{
    /// <summary>
    /// Creates a TCP client to connect to an RPC server listener. RPC is implemented as
    /// JSON-RPC over TCP (a web socket / network stream). Once the client connects, it can
    /// remain connected for the life of the TCP connection or until the server closes.
    /// The client should never be exposed to the naked internet (use security groups or
    /// virtual networks if splitting between two VMs).
    /// </summary>
    public class RpcClient
    {
        private static readonly ILogger logger = LogFactory.CreateLogger<RpcClient>();

        // Fields and Properties
        protected TcpClient BaseClient;
        protected JsonRpc Rpc;

        /// <summary>
        /// Connects the client to a remote TCP host using the specified host name and port
        /// number. Once the client has connected, the stream will be wrapped in a JSON-RPC
        /// protocol wrapper.
        /// </summary>
        /// <param name="address">IP address of the RPC server</param>
        /// <param name="port">Port the RPC server is listening on</param>
        /// <param name="agent">The name of the client</param>
        /// <returns>Returns a new task for connecting and fault tolerance.</returns>
        public async Task ConnectAsync(string address, int port, string agent = "Client")
        {
            while (true)
            {
                try
                {
                    BaseClient = new TcpClient();
                    await BaseClient.ConnectAsync(address, port);
                    using var stream = new NetworkStream(BaseClient.Client, true);
                    NetworkCredential networkCredential = new NetworkCredential("test", "test", "test.com");
                    

                    // Attach JSON-RPC wrapper
                    Rpc = JsonRpc.Attach(stream, stream);
                    await Rpc.InvokeAsync("ConnectedAsync", agent);
                    await Rpc.Completion;
                }
                catch (IOException) { }
                catch (SocketException) { }
                catch (Exception e) 
                { 
                    logger.LogError("Failed to connect to server. Error: {}", e.Message); 
                }
                Thread.Sleep(1000);
            }
        }

        public bool IsInitialized => BaseClient != null;

        /// <summary>
        /// Returns true if the RPC server is online and the client is connected.
        /// </summary>
        public bool Online => BaseClient.Connected;

        /// <summary>
        /// Invoke a method on the server and do not wait for a result.
        /// </summary>
        /// <param name="method">Name of the remote procedure method</param>
        /// <param name="arg">Argument to pass with the request</param>
        /// <typeparam name="T">Type of response returned by the procedure</typeparam>
        /// <returns>Returns a task of the running RPC invoke.</returns>
        public Task<T> CallAsync<T>(string method, object arg)
        {
            return Rpc.InvokeAsync<T>(method, arg);
        }

        public Task CallAsync(string method, object arg)
        {
            return Rpc.InvokeAsync(method, arg);
        }

        /// <summary>
        /// Invoke a method on the server and do not wait for a result.
        /// </summary>
        /// <param name="method">Name of the remote procedure method</param>
        /// <param name="args">Arguments to pass with the request</param>
        /// <typeparam name="T">Type of response returned by the procedure</typeparam>
        /// <returns>Returns a task of the running RPC invoke.</returns>
        public Task<T> CallAsync<T>(string method, params object[] args)
        {
            return Rpc.InvokeAsync<T>(method, args);
        }

        public Task CallAsync(string method, params object[] args)
        {
            return Rpc.InvokeAsync(method, args);
        }
    }
}
