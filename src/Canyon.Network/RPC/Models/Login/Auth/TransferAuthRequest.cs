﻿namespace Canyon.Network.RPC.Models.Login.Auth
{
    public class TransferAuthRequest
    {
        public string IPAddress { get; set; }
        public uint AccountID { get; set; }
        public ushort AuthorityID { get; set; }
        public DateTime LastLoginTime { get; set; }
    }
}
