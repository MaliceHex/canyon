﻿namespace Canyon.Network.RPC.Models.Login.Auth
{
    public class TransferAuthResponse
    {
        public ResultCode Code { get; set; }
        public string Message { get; set; }
        public ulong Token { get; set; }

        public enum ResultCode
        {
            Success = 0x0,
            Failed = 0x1
        }
    }
}
