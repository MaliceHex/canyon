﻿using Canyon.Database.Entities;
using Canyon.Game.Services.Managers;
using Canyon.Network.Packets.Login;
using Quartz;

namespace Canyon.Game.Threading
{
    [DisallowConcurrentExecution]
    public sealed class BasicThread : IJob
    {
        private static readonly ILogger logger = LogFactory.CreateLogger<BasicThread>();

        private const string CONSOLE_TITLE = "[{0}] Conquer Online Game Server - Players[{1}] Limit[{2}] Max[{3}] - {4} - Start: {8} - {5} - RoleTimerTicks[{6}] RoleCount[{7}]";

        private static readonly TimeOut accountReconnect = new(5);
        private static readonly TimeOut accountPing = new(15);
        private static readonly TimeOut accountSync = new();
        private static readonly DateTime serverStartTime;

        private static long lastUpdateTick = 0;

        static BasicThread()
        {
            serverStartTime = DateTime.Now;

            accountReconnect.Update();
            accountPing.Update();
            accountSync.Startup(60);
        }

        public async Task Execute(IJobExecutionContext context)
        {
            Console.Title = string.Format(CONSOLE_TITLE,
                ServerConfiguration.Configuration.Realm.Name,
                RoleManager.OnlinePlayers,
                ServerConfiguration.Configuration.Realm.MaxOnlinePlayers,
                RoleManager.MaxOnlinePlayers,
                DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss.fff"),
                Kernel.NetworkMonitor.UpdateStatsAsync((int)(Environment.TickCount - lastUpdateTick)),
                RoleManager.RoleTimerTicks,
                RoleManager.ProcessedRoles,
                serverStartTime.ToString("yyyy/MM/dd HH:mm:ss"));

            if (Kernel.Sockets.LoginClient != null && Kernel.Sockets.LoginClient.Online && accountSync.ToNextTime())
            {
                DbRealm.RealmStatus status = DbRealm.RealmStatus.Online;
                bool isBusy = RoleManager.OnlinePlayers > ServerConfiguration.Configuration.Realm.MaxOnlinePlayers / 2;
                if (isBusy)
                {
                    status = RoleManager.OnlinePlayers >= ServerConfiguration.Configuration.Realm.MaxOnlinePlayers ? DbRealm.RealmStatus.Full : DbRealm.RealmStatus.Busy;
                }

                await Kernel.Sockets.LoginClient.CallAsync("UpdateRealmInformationAsync", new MsgAccServerGameInformation
                {
                    PlayerCount = RoleManager.OnlinePlayers,
                    PlayerCountRecord = RoleManager.MaxOnlinePlayers,
                    PlayerLimit = ServerConfiguration.Configuration.Realm.MaxOnlinePlayers,
                    Status = (int)status
                });
            }

            await MaintenanceManager.OnTimerAsync();

            lastUpdateTick = Environment.TickCount;
        }

    }
}
