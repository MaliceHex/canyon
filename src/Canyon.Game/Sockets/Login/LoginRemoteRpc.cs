﻿using Canyon.Game.Services.Managers;
using Canyon.Game.States.Transfer;
using Canyon.Game.States.User;
using Canyon.Network.RPC;
using Canyon.Network.RPC.Models.Login.Auth;
using System.Runtime.Caching;
using System.Security.Cryptography;

namespace Canyon.Game.Sockets.Login
{
    public class LoginRemoteRpc : IRpcServerTarget
    {
        private static readonly ILogger logger = LogFactory.CreateLogger<LoginRemoteRpc>();

        public Task ConnectedAsync(string agentName)
        {
            logger.LogInformation("{} has connected", agentName);
            return Task.CompletedTask;
        }

        public TransferAuthResponse TransferAuth(TransferAuthRequest request)
        {
            Character user = RoleManager.GetUserByAccount(request.AccountID);
            if (user != null)
            {
                logger.LogInformation($"Login denied! User {request.AccountID} already signed in.");
                _ = RoleManager.KickOutAsync(user.Identity, "Duplicated login.").ConfigureAwait(false);
                return new TransferAuthResponse
                {
                    Code = TransferAuthResponse.ResultCode.Failed,
                    Message = "User already signed in."
                };
            }

            // Generate the access token
            var bytes = new byte[8];
            var rng = RandomNumberGenerator.Create();
            rng.GetBytes(bytes);
            var token = BitConverter.ToUInt64(bytes);

            var args = new TransferAuthArgs
            {
                AccountID = request.AccountID,
                AuthorityID = request.AuthorityID,
                IPAddress = request.IPAddress
            };
            // Store in the login cache with an absolute timeout
            var timeoutPolicy = new CacheItemPolicy { AbsoluteExpiration = DateTime.Now.AddSeconds(30) };
            Kernel.Logins.Set(token.ToString(), args, timeoutPolicy);

#if DEBUG
            logger.LogInformation($"Received Login Information for {request.AccountID}. Expiration: {timeoutPolicy.AbsoluteExpiration.ToLocalTime()}");
#endif

            return new TransferAuthResponse { Code = TransferAuthResponse.ResultCode.Success, Message= "OK", Token = token };
        }
    }
}
