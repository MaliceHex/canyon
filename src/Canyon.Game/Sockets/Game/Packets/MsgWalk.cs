﻿using Canyon.Game.Services.Managers;
using Canyon.Game.Sockets.Ai.Packets;
using Canyon.Game.States;
using Canyon.Game.States.User;
using Canyon.Network.Packets;
using Canyon.Network.Packets.Ai;

namespace Canyon.Game.Sockets.Game.Packets
{
    public sealed class MsgWalk : MsgBase<Client>
    {
        public uint Identity { get; set; }
        public byte Direction { get; set; }
        public byte Mode { get; set; }
        public int Timestamp { get; set; }
        public uint Map { get; set; }

        /// <summary>
        ///     Decodes a byte packet into the packet structure defined by this message class.
        ///     Should be invoked to structure data from the client for processing. Decoding
        ///     follows TQ Digital's byte ordering rules for an all-binary protocol.
        /// </summary>
        /// <param name="bytes">Bytes from the packet processor or client socket</param>
        public override void Decode(byte[] bytes)
        {
            using var reader = new PacketReader(bytes);
            Length = reader.ReadUInt16();
            Type = (PacketType)reader.ReadUInt16();
            Direction = (byte)reader.ReadUInt32();
            Identity = reader.ReadUInt32();
            Mode = reader.ReadByte();
            Timestamp = reader.ReadInt32();
            Map = reader.ReadUInt32();
        }

        /// <summary>
        ///     Encodes the packet structure defined by this message class into a byte packet
        ///     that can be sent to the client. Invoked automatically by the client's send
        ///     method. Encodes using byte ordering rules interoperable with the game client.
        /// </summary>
        /// <returns>Returns a byte packet of the encoded packet.</returns>
        public override byte[] Encode()
        {
            using var writer = new PacketWriter();
            writer.Write((ushort)PacketType.MsgWalk);
            writer.Write((int)Direction); // 4
            writer.Write(Identity); // 8
            writer.Write(Mode); // 12
            writer.Write(Timestamp);
            writer.Write(Map);
            return writer.ToArray();
        }

        public enum RoleMoveMode
        {
            Walk = 0,

            // PathMove()
            Run,
            Shift,

            // to server only
            Jump,
            Trans,
            Chgmap,
            JumpMagicAttack,
            Collide,
            Synchro,

            // to server only
            Track,

            RunDir0 = 20,

            RunDir7 = 27
        }

        /// <summary>
        ///     Process can be invoked by a packet after decode has been called to structure
        ///     packet fields and properties. For the server implementations, this is called
        ///     in the packet handler after the message has been dequeued from the server's
        ///     <see cref="PacketProcessor{TClient}" />.
        /// </summary>
        /// <param name="client">Client requesting packet processing</param>
        public override async Task ProcessAsync(Client client)
        {
            if (client != null && Identity == client.Character.Identity)
            {
                Character user = client.Character;
                await user.ProcessOnMoveAsync();

                bool moved = await user.MoveTowardAsync(Direction, Mode);
                Character couple;
                if (moved
                    && user.HasCoupleInteraction()
                    && user.HasCoupleInteractionStarted()
                    && (couple = user.GetCoupleInteractionTarget()) != null)
                {
                    await couple.ProcessOnMoveAsync();

                    couple.X = user.X;
                    couple.Y = user.Y;

                    await couple.ProcessAfterMoveAsync();

                    MsgSyncAction msg = new()
                    {
                        Action = SyncAction.Walk,
                        X = user.X,
                        Y = user.Y
                    };
                    msg.Targets.Add(user.Identity);
                    msg.Targets.Add(couple.Identity);

                    await user.SendAsync(this);
                    await user.ProcessAfterMoveAsync();
                    await BroadcastNpcMsgAsync(new MsgAiAction
                    {
                        Action = AiActionType.Walk,
                        Identity = user.Identity,
                        X = user.X,
                        Y = user.Y,
                        Direction = (int)user.Direction
                    });

                    Identity = couple.Identity;
                    await couple.SendAsync(this);
                    await couple.ProcessAfterMoveAsync();
                    await BroadcastNpcMsgAsync(new MsgAiAction
                    {
                        Action = AiActionType.Jump,
                        Identity = couple.Identity,
                        X = couple.X,
                        Y = couple.Y,
                        Direction = (int)couple.Direction
                    });

                    await user.SendAsync(msg);
                    await user.Screen.UpdateAsync(msg);
                    await couple.Screen.UpdateAsync();
                }
                else if (moved)
                {
                    await user.SendAsync(this);
                    await user.Screen.UpdateAsync(this);
                    await user.ProcessAfterMoveAsync();
                    await BroadcastNpcMsgAsync(new MsgAiAction
                    {
                        Action = AiActionType.Jump,
                        Identity = user.Identity,
                        X = user.X,
                        Y = user.Y,
                        Direction = (int)user.Direction
                    });
                }
                return;
            }

            Role target = RoleManager.GetRole(Identity);
            if (target == null)
            {
                return;
            }

            await target.ProcessOnMoveAsync();
            await target.MoveTowardAsync(Direction, Mode);
            if (target is Character targetUser)
            {
                await targetUser.Screen.UpdateAsync(this);
            }
            else
            {
                await target.BroadcastRoomMsgAsync(this, false);
            }
            await target.ProcessAfterMoveAsync();
            await BroadcastNpcMsgAsync(new MsgAiAction
            {
                Action = AiActionType.Jump,
                Identity = target.Identity,
                X = target.X,
                Y = target.Y,
                Direction = (int)target.Direction
            });
        }
    }
}
