﻿using Canyon.Game.Sockets.Ai.Packets;
using Canyon.Game.Sockets.Game.Packets;
using Canyon.Network.Packets.Ai;
using System.Drawing;
using System.Globalization;

namespace Canyon.Game.Services.Managers
{
    public class MaintenanceManager
    {
        private static readonly ILogger logger = LogFactory.CreateLogger<MaintenanceManager>();

        private static TimeOut timeOut = new TimeOut();
        private static int minutesToShutdown = 0;
        private static int currentAlerts = 0;

        private MaintenanceManager() { }

        internal static MaintenanceObject NextSchedule;

        private static DateTime GetNextWeekday(DateTime start, DayOfWeek day)
        {
            // The (... + 7) % 7 ensures we end up with a value in the range [0, 6]
            int daysToAdd = ((int)day - (int)start.DayOfWeek + 7) % 7;
            return start.AddDays(daysToAdd);
        }

        public static Task InitializeAsync()
        {
            logger.LogInformation("Initializing Maintenance Manager!");

            if (ServerConfiguration.Configuration.MaintenanceSchedule.Count == 0)
            {
                return Task.CompletedTask;
            }

            Dictionary<DateTime, ServerConfiguration.MaintenanceScheduleConfiguration> schedules = new();
            foreach (var schedule in ServerConfiguration.Configuration.MaintenanceSchedule)
            {
                DateTime nextSchedule = GetNextWeekday(DateTime.Now, schedule.DayOfWeek);
                nextSchedule = DateTime.ParseExact($"{nextSchedule:yyyy-MM-dd} {schedule.Time}", "yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture);
                schedules.Add(nextSchedule, schedule);
            }
            //
            var next = schedules.Where(x => x.Key > DateTime.Now).OrderBy(x => x.Key).First();
            NextSchedule = new MaintenanceObject(next.Value, next.Key);

            logger.LogInformation($"Next maintenance set for: {NextSchedule.NextSchedule}");
            return Task.CompletedTask;
        }

        public static async Task AnnounceMaintenanceAsync(int minutes = 5)
        {
            minutesToShutdown = Math.Max(1, minutes);
            currentAlerts = 1;

            RoleManager.SetMaintenanceStart();

            await BroadcastNpcMsgAsync(new MsgAiAction
            {
                Action = AiActionType.Shutdown
            }, 
            () =>
            {
                Kernel.SetMaintenance();
                return Task.CompletedTask;
            });

            await BroadcastWorldMsgAsync(string.Format(StrServerMaintenanceScheduleStart, minutesToShutdown), TalkChannel.Talk, Color.White);
            timeOut.Startup(30);
        }

        private static async Task OnCloseServerAsync()
        {
            await RoleManager.KickOutAllAsync(StrServerMaintenanceShutdown, true);
            await Kernel.StopAsync();
            Environment.Exit(0);
        }

        public static async Task OnTimerAsync()
        {
            if (minutesToShutdown == 0)
            {
                if (NextSchedule != null && NextSchedule.WarningTime <= DateTime.Now)
                {
                    await AnnounceMaintenanceAsync(NextSchedule.Minutes);
                }
                return;
            }

            if (!timeOut.ToNextTime())
            {
                return;
            }

            int remainingAlerts = (minutesToShutdown * 2) - currentAlerts;
            if (remainingAlerts > 0)
            {
                string message;
                if (remainingAlerts == 1) // last one, 30 seconds
                {
                    message = string.Format(StrServerMaintenanceSeconds, 30);
                }
                else if (remainingAlerts == 2) // 1 minute
                {
                    message = string.Format(StrServerMaintenanceMinute, 1);
                }
                else if (remainingAlerts == 3) // 1 minute and 30 seconds
                {
                    message = string.Format(StrServerMaintenanceMinuteSeconds, 1, 30);
                }
                else if (currentAlerts % 2 == 0) // N minutes
                {
                    message = string.Format(StrServerMaintenanceMinutes, (remainingAlerts / 2));
                }
                else // N minutes and 30 seconds
                {
                    message = string.Format(StrServerMaintenanceMinutesSeconds, (remainingAlerts / 2), 30);
                }

                await BroadcastWorldMsgAsync(string.Format(StrServerMaintenanceWarning, message), TalkChannel.Center, Color.White);
                currentAlerts++;
                return;
            }

            await OnCloseServerAsync();
        }

        internal class MaintenanceObject
        {
            private readonly ServerConfiguration.MaintenanceScheduleConfiguration configuration;
            private readonly DateTime nextSchedule;

            public MaintenanceObject(ServerConfiguration.MaintenanceScheduleConfiguration configuration, DateTime nextSchedule) 
            {
                this.configuration = configuration;
                this.nextSchedule = nextSchedule;

                WarningTime = nextSchedule.AddMinutes(configuration.WarningMinutes * -1);
            }

            public int Minutes => configuration.WarningMinutes;
            public DateTime WarningTime { get; init; }
            public DateTime NextSchedule => nextSchedule;
        }
    }
}
