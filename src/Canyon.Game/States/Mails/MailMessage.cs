using Canyon.Database.Entities;
using Canyon.Game.Database;

namespace Canyon.Game.States.Mails
{
    public sealed class MailMessage
    {
        [Flags]
        public enum MessageFlag : byte
        {
            None = 0,
            Read = 0x1,
            Deleted = 0x2,
            MoneyClaimed = 0x4,
            EmoneyClaimed = 0x8,
            ItemClaimed = 0x10,
            Notified = 0x20,
            ActionClaimed = 0x40
        }

        private readonly DbMail mail;

        public MailMessage(DbMail mail)
        {
            this.mail = mail;
        }

        private MessageFlag Flag => (MessageFlag)mail.Flag;

        public bool IsRead => (Flag & MessageFlag.Read) != 0;
        public bool IsDeleted => (Flag & MessageFlag.Deleted) != 0;
        public bool HasClaimedMoney => (Flag & MessageFlag.MoneyClaimed) != 0 && Money != 0;
        public bool HasClaimedConquerPoints => (Flag & MessageFlag.EmoneyClaimed) != 0 && ConquerPoints != 0;
        public bool HasClaimedItem => (Flag & MessageFlag.ItemClaimed) != 0 && (Item != 0 || ItemType != 0);
        public bool HasClaimedAction => (Flag & MessageFlag.ActionClaimed) != 0 && Action != 0;
        public bool IsNotified => (Flag & MessageFlag.Notified) != 0;
        public bool HasExpired => UnixTimestamp.Now > Expiration;

        public int Order => !IsRead || !HasClaimedConquerPoints || !HasClaimedItem || !HasClaimedMoney ? 1 : 0;

        public uint Identity => mail.Id;
        public string SenderName => mail.SenderName;
        public string Title => mail.Title;
        public string Content => mail.Content;

        public ulong Money => ContainsFlag(MessageFlag.MoneyClaimed) ? 0 : mail.Money;
        public uint ConquerPoints => ContainsFlag(MessageFlag.EmoneyClaimed) ? 0 : mail.ConquerPoints;
        public uint Item => ContainsFlag(MessageFlag.ItemClaimed) ? 0 : mail.ItemId;
        public uint ItemType => ContainsFlag(MessageFlag.ItemClaimed) ? 0 : mail.ItemType;
        public uint Action => ContainsFlag(MessageFlag.ActionClaimed) ? 0 : mail.Action;
        public uint Expiration => mail.ExpirationDate;


        public Task SetFlagAsync(MessageFlag flag)
        {
            mail.Flag |= (byte)flag;
            return ServerDbContext.SaveAsync(mail);
        }

            public bool ContainsFlag(MessageFlag flag)
        => (mail.Flag & (byte)flag) == (byte)flag;
    }
}
