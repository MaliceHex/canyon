﻿using Canyon.Database.Entities;
using Canyon.Game.Database;
using Canyon.Game.Database.Repositories;
using Canyon.Game.Sockets.Game.Packets;
using Canyon.Game.States.Events.Tournament;
using Canyon.Game.States.User;
using System.Collections.Concurrent;

namespace Canyon.Game.States.Events.Elite
{
    public sealed class ElitePkGroupTournament : GameTournamentEvent<ElitePkParticipant, Character>
    {
        private readonly uint waitingMapId;
        private const uint DYNA_RANK_REC_TYPE = 1_000_000_000;

        private readonly int index;
        private readonly ConcurrentDictionary<uint, DbDynaRankRec> rankings = new ConcurrentDictionary<uint, DbDynaRankRec>();

        public ElitePkGroupTournament(TournamentRules tournamentRules, uint waitingMapId, int index)
            : base(tournamentRules)
        {
            this.waitingMapId = waitingMapId;
            this.index = index;
        }

        public async Task InitializeAsync()
        {
            var rankings = await DynaRankRecRepository.GetAsync(DYNA_RANK_REC_TYPE + waitingMapId);
            // let's initialize the array with 8 items
            if (rankings.Count < 8)
            {
                if (rankings.Count > 0)
                {
                    await ServerDbContext.DeleteRangeAsync(rankings);
                    rankings.Clear();
                }

                for (int i = rankings.Count; i < 8; i++)
                {
                    rankings.Add(new DbDynaRankRec
                    {
                        RankType = DYNA_RANK_REC_TYPE + waitingMapId,
                        Value = i + 1,
                        ObjId = 0,
                        UserId = 0,
                        Time = 0,
                        ServerId = 0,
                        UserName = StrNone
                    });
                }
                await ServerDbContext.SaveRangeAsync(rankings);
            }

            foreach (var ranking in rankings)
            {
                this.rankings.TryAdd((uint)ranking.Value, ranking);
            }
        }

        public override Task<bool> InscribeAsync(ElitePkParticipant entity)
        {
            throw new NotImplementedException();
        }

        public override bool IsContestant(uint identity)
        {
            return participants.Any(x => x.Identity == identity);
        }

        public override bool IsParticipantAllowedToJoin(Character participant)
        {
            if (participant.Level < tournamentRules.MinLevel)
            {
                return false;
            }
            if (participant.Level > tournamentRules.MaxLevel)
            {
                return false;
            }
            if (participant.Metempsychosis < tournamentRules.Metempsychosis)
            {
                return false;
            }
            if (participant.NobilityRank < tournamentRules.MinNobility)
            {
                return false;
            }
            if (participant.NobilityRank > tournamentRules.MaxNobility)
            {
                return false;
            }
            return true;
        }

        public override Task SubmitEventWindowAsync(Character target)
        {
            MsgElitePKGameRankInfo msg = new MsgElitePKGameRankInfo
            {
                Group = index
            };
            var stage = GetStage();
            switch (stage)
            {
                case TournamentStage.None:
                    {
                        foreach (var r in rankings.Values.OrderBy(x => x.Value))
                        {
                            msg.Rank.Add(new MsgElitePKGameRankInfo.ElitePkRankStruct
                            {
                                Rank = (int)(r.Value - 1),
                                Identity = r.UserId,
                                Mesh = r.ObjId,
                                Name = r.UserName
                            });
                        }
                        break;
                    }

                case TournamentStage.Knockout:
                    {
                        break;
                    }

                case TournamentStage.Knockout8:
                    {
                        break;
                    }

                case TournamentStage.QuarterFinals:
                    {
                        break;
                    }

                case TournamentStage.SemiFinals:
                    {
                        break;
                    }

                case TournamentStage.ThirdPlace:
                    {
                        break;
                    }

                case TournamentStage.Finals:
                    {
                        break;
                    }
            }
            return target.SendAsync(msg);
        }

        public override Task UnsubscribeAsync(ElitePkParticipant entity)
        {
            throw new NotImplementedException();
        }

        public override Task UnsubscribeAsync(uint identity)
        {
            throw new NotImplementedException();
        }

        
    }
}
