﻿using Canyon.Game.States.Events.Interfaces;

namespace Canyon.Game.States.Events.Tournament
{
    public class SingleTournamentMatch<TParticipant, TEntity>
        : BaseTournamentMatch<TParticipant, TEntity>
        where TParticipant : ITournamentEventParticipant<TEntity>
    {
        public SingleTournamentMatch(int identity, int index)
            : base(identity, index)
        {

        }
    }
}
