﻿#define DEBUG_TOURNAMENT

using Canyon.Game.Services.Managers;
using Canyon.Game.Sockets.Game.Packets;
using Canyon.Game.States.Events.Interfaces;
using Canyon.Game.States.User;

namespace Canyon.Game.States.Events.Tournament
{
    public abstract class GameTournamentEvent<TParticipant, TEntity>
        : ITournamentEvent<TParticipant, TEntity>
        where TParticipant : ITournamentEventParticipant<TEntity>
    {
        private static readonly ILogger logger = LogFactory.CreateLogger<GameTournamentEvent<TParticipant, TEntity>>();

        private static readonly Random random = new(); // not using the async one here

        protected readonly TournamentRules tournamentRules;

        protected bool firstStagePlayed = false;
        protected bool singleStageOnly = false;

        protected List<TournamentParticipantReward<TParticipant, TEntity>> tournamentParticipantReward = new();
        protected List<TParticipant> participants = new();

        protected List<BaseTournamentMatch<TParticipant, TEntity>> matches = new();
        protected List<BaseTournamentMatch<TParticipant, TEntity>> previousMatches = new();

        protected BaseTournamentMatch<TParticipant, TEntity> thirdPlace;
        protected BaseTournamentMatch<TParticipant, TEntity> firstPlace;

        public GameTournamentEvent()
        {
            tournamentRules = new TournamentRules
            {
                MinLevel = 1,
                MaxLevel = ExperienceManager.GetLevelLimit(),
                MinNobility = MsgPeerage.NobilityRank.Serf,
                MaxNobility = MsgPeerage.NobilityRank.King,
                Metempsychosis = 0
            };
        }

        public GameTournamentEvent(TournamentRules tournamentRules)
        {
            this.tournamentRules = tournamentRules;
        }

        public TournamentRules Rules => tournamentRules;

        public int CurrentMatchIdentity { get; protected set; }
        public int CurrentMatchIndex { get; protected set; }

        public abstract Task<bool> InscribeAsync(TParticipant entity);
        public abstract Task SubmitEventWindowAsync(Character target);
        public abstract Task UnsubscribeAsync(TParticipant entity);
        public abstract Task UnsubscribeAsync(uint identity);
        public abstract bool IsContestant(uint identity); // user

        public abstract bool IsParticipantAllowedToJoin(TEntity participant);

        public void Start()
        {
            GenerateMatches();
        }

        public void Step()
        {
            bool generateBrackets = true;
            TournamentStage currentStage = GetStage();
            foreach (var match in matches)
            {
                if (match is SingleTournamentMatch<TParticipant, TEntity> singleMatch)
                {
                    TParticipant loser;
                    if (!singleMatch.Finished)
                    {
                        if (singleMatch.Score1 > singleMatch.Score2)
                        {
                            singleMatch.Winner = singleMatch.Participant1;
                            loser = singleMatch.Participant2;
                            participants.RemoveAll(x => x.Identity == singleMatch.Participant1?.Identity);
                        }
                        else
                        {
                            singleMatch.Winner = singleMatch.Participant2;
                            loser = singleMatch.Participant1;
                            participants.RemoveAll(x => x.Identity == singleMatch.Participant2?.Identity);
                        }
                    }
                    else
                    {
                        loser = singleMatch.Participant1?.Identity == singleMatch.Winner?.Identity ? singleMatch.Participant2 : singleMatch.Participant1;
                    }

                    if (currentStage == TournamentStage.SemiFinals)
                    {
                        if (firstPlace == null)
                        {
                            firstPlace = new SingleTournamentMatch<TParticipant, TEntity>(CurrentMatchIdentity++, CurrentMatchIndex++)
                            {
                                Participant1 = singleMatch.Winner,
                                Stage = TournamentStage.Finals
                            };
                        }
                        else
                        {
                            firstPlace.Participant2 = singleMatch.Winner;
                        }

                        if (thirdPlace == null)
                        {
                            thirdPlace = new SingleTournamentMatch<TParticipant, TEntity>(CurrentMatchIdentity++, CurrentMatchIndex++)
                            {
                                Participant1 = loser,
                                Stage = TournamentStage.Finals
                            };
                        }
                        else
                        {
                            thirdPlace.Participant2 = loser;
                        }
                    }
                    else if (currentStage == TournamentStage.ThirdPlace)
                    {
                        tournamentParticipantReward.Add(new TournamentParticipantReward<TParticipant, TEntity>(thirdPlace.Winner, TournamentParticipantRank.Top3));
                        tournamentParticipantReward.Add(new TournamentParticipantReward<TParticipant, TEntity>(loser, TournamentParticipantRank.Top8));
                        thirdPlace = null;
                    }
                    else if (currentStage == TournamentStage.Finals)
                    {
                        tournamentParticipantReward.Add(new TournamentParticipantReward<TParticipant, TEntity>(firstPlace.Winner, TournamentParticipantRank.Champion));
                        tournamentParticipantReward.Add(new TournamentParticipantReward<TParticipant, TEntity>(loser, TournamentParticipantRank.Top2));
                        firstPlace = null;
                    }
                    else
                    {
                        tournamentParticipantReward.Add(new TournamentParticipantReward<TParticipant, TEntity>(loser, TournamentParticipantRank.Top8));
                    }

                    match.Finished = true;
                    previousMatches.Add(match);
                }
                else
                {
                    DoubleTournamentMatch<TParticipant, TEntity> doubleMatch = match as DoubleTournamentMatch<TParticipant, TEntity>;
                    if (!firstStagePlayed)
                    {
                        singleMatch = doubleMatch.Match1;
                        generateBrackets = false;
                    }
                    else
                    {
                        singleMatch = doubleMatch.Match2;
                        match.Finished = true;
                    }

                    TParticipant loser;
                    if (!singleMatch.Finished)
                    {
                        if (singleMatch.Score1 > singleMatch.Score2)
                        {
                            singleMatch.Winner = singleMatch.Participant1;
                            loser = singleMatch.Participant2;
                            participants.RemoveAll(x => x.Identity == singleMatch.Participant1?.Identity);
                        }
                        else
                        {
                            singleMatch.Winner = singleMatch.Participant2;
                            loser = singleMatch.Participant1;
                            participants.RemoveAll(x => x.Identity == singleMatch.Participant2?.Identity);
                        }
                    }
                    else
                    {
                        loser = singleMatch.Participant1?.Identity == singleMatch.Winner?.Identity ? singleMatch.Participant2 : singleMatch.Participant1;
                    }
                    singleMatch.Finished = true;
                    previousMatches.Add(match);
                }
            }

            if (!firstStagePlayed && matches.All(x => x is DoubleTournamentMatch<TParticipant, TEntity>))
            {
                firstStagePlayed = true;
            }

            matches.Clear();

            currentStage = GetStage();
            if (currentStage == TournamentStage.QuarterFinals)
            {
                var tempParticipants = new List<TParticipant>(participants);
                participants.Clear();

                do
                {
                    int idx = random.Next(tempParticipants.Count) % tempParticipants.Count;
                    participants.Add(tempParticipants[idx]);
                    tempParticipants.RemoveAt(idx);
                }
                while (tempParticipants.Count > 0);
            }

            if (generateBrackets || singleStageOnly)
            {
                GenerateMatches();
            }
        }

        public void GenerateMatches()
        {
            TournamentStage currentStage = GetStage();
            logger.LogDebug($"Generating matches for stage: {currentStage}");
            List<TParticipant> participants = this.participants.ToList();
            if (!IsDoubleMatchStage())
            {
                if (thirdPlace != null)
                {
                    matches.Add(thirdPlace);
                }
                else if (firstPlace != null)
                {
                    matches.Add(firstPlace);
                }
                else
                {
                    if (participants.Count > 8)
                    {
                        do
                        {
                            int idx = random.Next(participants.Count) % participants.Count;
                            TParticipant p1 = participants[idx];
                            participants.RemoveAt(idx);

                            TParticipant p2 = default;
                            if (participants.Count > 0)
                            {
                                idx = random.Next(participants.Count) % participants.Count;
                                p2 = participants[idx];
                                participants.RemoveAt(idx);
                            }

                            SingleTournamentMatch<TParticipant, TEntity> gameMatch = new SingleTournamentMatch<TParticipant, TEntity>(CurrentMatchIdentity++, CurrentMatchIndex++);
                            gameMatch.Participant1 = p1;
                            gameMatch.Participant2 = p2;
                            gameMatch.Stage = currentStage;
                            matches.Add(gameMatch);
                        }
                        while (participants.Count > 0);
                    }
                    else
                    {
                        int matchAmount = 4;
                        if (currentStage == TournamentStage.SemiFinals)
                        {
                            matchAmount = 2;
                        }

                        int idx = 0;
                        do
                        {
                            int[] quarterFinals =
                            {
                                0, 2, 1, 3
                            };
                            SingleTournamentMatch<TParticipant, TEntity> match;
                            if (matches.Count >= (idx % matchAmount) + 1)
                            {
                                int matchIdx = currentStage == TournamentStage.QuarterFinals ? quarterFinals[idx % matchAmount] : idx % matchAmount;
                                match = matches[matchIdx] as SingleTournamentMatch<TParticipant, TEntity>;
                            }
                            else
                            {
                                match = new SingleTournamentMatch<TParticipant, TEntity>(CurrentMatchIdentity++, CurrentMatchIndex++)
                                {
                                    Stage = currentStage
                                };
                                matches.Add(match);
                            }

                            if (match.Participant1 == null)
                            {
                                match.Participant1 = participants[0];
                            }
                            else
                            {
                                match.Participant2 = participants[0];
                            }

                            participants.RemoveAt(0);
                            idx++;
                        }
                        while (participants.Count > 0);
                    }
                }
            }
            else
            {
                int idx = 0;
                do
                {
                    int rand = random.Next(participants.Count) % participants.Count;
                    DoubleTournamentMatch<TParticipant, TEntity> doubleMatch;
                    if (matches.Count >= (idx % 8) + 1)
                    {
                        doubleMatch = matches[idx % 8] as DoubleTournamentMatch<TParticipant, TEntity>;
                    }
                    else
                    {
                        doubleMatch = new DoubleTournamentMatch<TParticipant, TEntity>(CurrentMatchIdentity++, CurrentMatchIndex++);
                        doubleMatch.Stage = currentStage;
                    }

                    if (doubleMatch == null)
                    {
                        throw new NullReferenceException();
                    }

                    TParticipant participant = participants[rand];
                    if (doubleMatch.Participant1 == null)
                    {
                        doubleMatch.Participant1 = participant;
                    }
                    else if (doubleMatch.Participant2 == null)
                    {
                        doubleMatch.Participant2 = participant;
                        doubleMatch.SetFirstMatch();
                    }
                    else
                    {
                        doubleMatch.SetSecondMatch(participant);
                    }

                    participants.RemoveAt(rand);
                    idx++;
                }
                while (participants.Count > 0);

                if (this.participants.Count <= 16)
                {
                    singleStageOnly = true;
                }
            }
        }

        public bool IsDoubleMatchStage()
        {
            return GetStage() == TournamentStage.Knockout8;
        }

        public TournamentStage GetStage()
        {
            if (participants.Count > 24)
            {
                return TournamentStage.Knockout;
            }
            if (participants.Count > 8)
            {
                return TournamentStage.Knockout8;
            }
            if (participants.Count > 4)
            {
                return TournamentStage.QuarterFinals;
            }
            if (participants.Count > 2)
            {
                return TournamentStage.SemiFinals;
            }
            if (participants.Count > 1)
            {
                if (thirdPlace != null)
                {
                    return TournamentStage.ThirdPlace;
                }
                if (firstPlace != null)
                {
                    return TournamentStage.Finals;
                }
            }
            return TournamentStage.Final;
        }
    }
}
