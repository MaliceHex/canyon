﻿using Canyon.Database.Entities;
using Canyon.Game.Database;
using Canyon.Game.Database.Repositories;
using Canyon.Game.States.User;
using System.Collections.Concurrent;

namespace Canyon.Game.States
{
    public sealed class UserStatistic
    {
        private readonly ConcurrentDictionary<ulong, DbStatistic> statistics = new();

        private readonly Character user;

        public UserStatistic(Character user)
        {
            this.user = user;
        }

        public async Task<bool> InitializeAsync()
        {
            try
            {
                var list = await StatisticRepository.GetAsync(user.Identity);
                if (list != null)
                {
                    foreach (var st in list)
                    {
                        statistics.TryAdd(GetKey(st.EventType, st.DataType), st);
                    }
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> AddOrUpdateAsync(uint idEvent, uint idType, uint data, bool bUpdate)
        {
            ulong key = GetKey(idEvent, idType);
            if (statistics.TryGetValue(key, out var stc))
            {
                stc.Data = data;
                if (bUpdate)
                {
                    stc.Timestamp = DateTime.Now;
                }
            }
            else
            {
                stc = new DbStatistic
                {
                    Data = data,
                    DataType = idType,
                    EventType = idEvent,
                    PlayerIdentity = user.Identity,
                    Timestamp = DateTime.Now
                };
                statistics.TryAdd(key, stc);
            }

            return await ServerDbContext.SaveAsync(stc);
        }

        public async Task<bool> SetTimestampAsync(uint idEvent, uint idType, DateTime? data)
        {
            DbStatistic stc = GetStc(idEvent, idType);
            if (stc == null)
            {
                await AddOrUpdateAsync(idEvent, idType, 0, true);
                stc = GetStc(idEvent, idType);

                if (stc == null)
                {
                    return false;
                }
            }

            stc.Timestamp = data;
            return await ServerDbContext.SaveAsync(stc);
        }

        public uint GetValue(uint idEvent, uint idType = 0)
        {
            return statistics.FirstOrDefault(x => x.Key == GetKey(idEvent, idType)).Value?.Data ?? 0u;
        }

        public DbStatistic GetStc(uint idEvent, uint idType = 0)
        {
            return statistics.FirstOrDefault(x => x.Key == GetKey(idEvent, idType)).Value;
        }

        public bool HasEvent(uint idEvent, uint idType)
        {
            return statistics.ContainsKey(GetKey(idEvent, idType));
        }

        private ulong GetKey(uint idEvent, uint idType)
        {
            return idEvent + ((ulong)idType << 32);
        }

        public async Task<bool> SaveAllAsync()
        {
            return await ServerDbContext.SaveAsync(statistics.Values.ToList());
        }
    }
}
