﻿using Canyon.Database.Entities;

namespace Canyon.Game.Database.Repositories
{
    public static class StatisticRepository
    {
        public static async Task<List<DbStatistic>> GetAsync(uint idUser)
        {
            await using var db = new ServerDbContext();
            return db.Statistics.Where(x => x.PlayerIdentity == idUser).ToList();
        }
    }
}
