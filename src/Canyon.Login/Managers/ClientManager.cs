﻿using Canyon.Login.States;
using System.Collections.Concurrent;

namespace Canyon.Login.Managers
{
    public static class ClientManager
    {
        private static ConcurrentDictionary<uint, Client> Clients { get; } = new ConcurrentDictionary<uint, Client>();

        public static bool AddClient(Client client)
        {
            return Clients.TryAdd(client.AccountID, client);
        }

        public static bool HasClient(uint idClient) => Clients.ContainsKey(idClient);

        public static Client GetClient(uint idClient) => Clients.TryGetValue(idClient, out var result) ? result : null;

        public static bool RemoveClient(uint idClient)
        {
            return Clients.TryRemove(idClient, out _);
        }
    }
}
