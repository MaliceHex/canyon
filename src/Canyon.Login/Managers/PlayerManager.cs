﻿using Canyon.Login.States;
using Canyon.Shared;
using Microsoft.Extensions.Logging;
using System.Collections.Concurrent;

namespace Canyon.Login.Managers
{
    public class PlayerManager
    {
        private static readonly ILogger logger = LogFactory.CreateLogger<PlayerManager>();

        private static readonly ConcurrentDictionary<uint, Player> players = new();

        private PlayerManager() { }

        public static int Count => players.Count;

        public static bool LoginUser(Player player)
        {
            return players.TryAdd(player.AccountId, player);
        }

        public static Player GetPlayer(uint accountId)
        {
            return players.TryGetValue(accountId, out var player) ? player : null;
        }

        public static void RemoveUser(uint accountId)
        {
            players.TryRemove(accountId, out _);
        }

        public static void RemoveAllFromRealm(Guid realmId)
        {
            List<uint> removePlayers = players.Values.Where(x => x.RealmId == realmId).Select(x => x.AccountId).ToList();
            foreach (var p in removePlayers)
            {
                players.TryRemove(p, out _);
            }
        }

    }
}
