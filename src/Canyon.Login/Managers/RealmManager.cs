﻿using Canyon.Login.Repositories;
using Canyon.Login.States;
using Canyon.Network.RPC;
using Canyon.Shared;
using Microsoft.Extensions.Logging;
using System.Collections.Concurrent;

namespace Canyon.Login.Managers
{
    public class RealmManager
    {
        private static readonly ILogger logger = LogFactory.CreateLogger<RealmManager>();

        private static readonly ConcurrentDictionary<Guid, Realm> Realms = new();

        private RealmManager() { }

        public static int Count => Realms.Count;

        public static async Task InitializeAsync()
        {
            logger.LogInformation("Starting realm manager");
            var realms = await RealmsRepository.QueryRealmsAsync();
            foreach (var r in realms)
            {
                var realmResponse = await RealmsRepository.FindByIdAsync(r.RealmID.ToString());
                if (realmResponse == null)
                {
                    logger.LogInformation("Failed to fetch '{}' [ID:{}] data", r.RealmName, r.RealmID);
                    continue;
                }

#if DEBUG
                if (!realmResponse.GameIPAddress.StartsWith("192.168.")
                    && !realmResponse.GameIPAddress.StartsWith("127."))
                {
                    logger.LogDebug("DEBUG mode, do not connect to external addressess.");
                    continue;
                }
#endif

                var realm = new Realm
                {
                    Data = realmResponse,
                    Rpc = new RpcClient()
                };
                _ = realm.Rpc.ConnectAsync(realmResponse.RpcIPAddress, (int)realmResponse.RpcPort, "Account Server");
                AddRealm(realm);
            }
        }

        public static bool AddRealm(Realm realm)
        {
            logger.LogInformation("New realm {} being added", realm.RealmID);
            return Realms.TryAdd(realm.RealmID, realm);
        }

        public static bool HasRealm(Guid idRealm) => Realms.ContainsKey(idRealm);

        public static Realm GetRealm(string name) => Realms.Values.FirstOrDefault(x => x.Data.RealmName.Equals(name));
        public static Realm GetRealm(Guid idRealm) => Realms.TryGetValue(idRealm, out var result) ? result : null;

        public static bool RemoveRealm(Guid idRealm)
        {
            return Realms.TryRemove(idRealm, out _);
        }
    }
}
